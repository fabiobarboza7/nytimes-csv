import { Header } from './components/Header';
import { Home } from './page';
import GlobalStyles from './styles/global';

const App = () => (
  <>
    <GlobalStyles />
    <Header />
    <Home />
  </>
);

export default App;
