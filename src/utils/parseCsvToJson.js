export default function parseCsvToJson(csv) {
  const arrayData = csv.split('\n');
  arrayData.shift();

  return arrayData;
}