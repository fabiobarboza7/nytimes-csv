import { Loading } from '../components/Loading';
import { StyledTable } from '../components/Table';
import { useCsv } from '../hooks/useCsv.hook'
import { Container } from './styles';

function Home() {
  const {handleCsvSize, handleCsvSort, sortedArray, loading} = useCsv();

  return (
    <Container>
      {console.log(sortedArray)}
      <Loading isLoading={loading} />
      <div>
        <select onChange={handleCsvSize}>
          <option value="0" disabled defaultValue="0">Select a limit</option>
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="100">100</option>
          <option value="1000">1000</option>
          <option value="10000">10000</option>
        </select>
        <select onChange={handleCsvSort}>
          <option value="0" disabled defaultValue="0">Select sort type</option>
          <option value="county">county</option>
          <option value="state">state</option>
          <option value="never">never</option>
          <option value="rarely">rarely</option>
          <option value="sometimes">sometimes</option>
          <option value="frequently">frequently</option>
          <option value="always">always</option>

        </select>
      </div>
      <p>Founded: {sortedArray.length} results</p>
      <StyledTable data={sortedArray} />
    </Container>
  )
}

export { Home }