import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: calc(100vh - 100px);
  
  select {
    height: 40px;
    border-radius: 4px;
    background: white;
    margin: 10px;
  }

  p {
    font-weight: bold;
    margin: 10px 0;
    color: tomato;
  }
`;