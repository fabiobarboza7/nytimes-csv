import styled from 'styled-components';

export const Container = styled.header`
  color: white;
  padding: 20px;
  background: linear-gradient(45deg, purple, tomato);
`;