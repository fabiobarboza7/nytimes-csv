import { Container } from "./styles"

function Header() {
  return (
    <Container>
      <h1>Covid-19 Data</h1>
    </Container>
  )
}

export { Header }