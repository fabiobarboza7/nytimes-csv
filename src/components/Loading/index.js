import { Container } from "./styles"

function Loading({ isLoading }) {
  return isLoading && (
    <Container>
      <p>Loading ...</p>
    </Container>
  )
}

export { Loading }