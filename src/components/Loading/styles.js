import styled from 'styled-components';

export const Container = styled.div`
  background-color: black;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100vh;
  opacity: 0.8;

  display: flex;
  justify-content: center;
  align-items: center;

  font-size: 28px;
  color: white;
`;