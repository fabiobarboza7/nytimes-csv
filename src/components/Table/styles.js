import styled from 'styled-components';

export const Container = styled.div`
  max-height: 500px;
  overflow-y: scroll;
`;

export const Table = styled.table`
  border-spacing: 0;
  border: 1px solid purple;
  color: white;

  tr {
    :last-child {
      td {
        border-bottom: 0;
      }
    }
  }

  th,
  td {
    padding: 0.5rem;
    border-bottom: 1px solid purple;
    border-right: 1px solid purple;

    :last-child {
      border-right: 0;
    }
  }

  th {
    background: purple;
    color: white;
    font-weight: bold;
  }
`;