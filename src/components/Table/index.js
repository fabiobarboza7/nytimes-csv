import { Container, Table } from "./styles"

function StyledTable({ data }) {
  return (
    <Container>
      <Table>
        <thead>
          <tr>
            <th>County</th>
            <th>State</th>
            <th>Never</th>
            <th>Rarely</th>
            <th>Sometimes</th>
            <th>Frequently</th>
            <th>Always</th>
          </tr>
        </thead>
        <tbody>
          {data.map((each, index) => (
            <tr key={index.toString()}>
              <td>{each.county}</td>
              <td>{each.state}</td>
              <td>{each?.never}</td>
              <td>{each?.rarely}</td>
              <td>{each?.sometimes}</td>
              <td>{each?.frequently}</td>
              <td>{each?.always}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  )
}

export { StyledTable }