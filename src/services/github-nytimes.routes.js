import { api } from './github-nytimes.api';

const csvData = async () => {
  const { data } = await api.get('/covid-19-data/master/us-counties.csv');
  
  return data;
}

const csvDataMask = async () => {
  const { data } = await api.get('/covid-19-data/master/mask-use/mask-use-by-county.csv');

  return data;
}

export {csvData, csvDataMask};