import { useEffect, useMemo, useState } from 'react';
import { csvData, csvDataMask } from '../services/github-nytimes.routes';
import parseCsvToJson from '../utils/parseCsvToJson'

function useCsv() {
  const [loading, setLoading] = useState(false);
  const [csv, setCsv] = useState([]);
  const [size, setSize] = useState(5);
  const [selectedSort, setSelectedSort] = useState('county');

  useEffect(() => {
    async function getCsvData() {
      setLoading(true);

      const csvResponse = await csvData();
      const csvResponseMask = await csvDataMask();
      
      const combineData = [...parseCsvToJson(csvResponse)].slice(0, size).map((eachLine) => {
        const [, county, state, fips] = eachLine.split(',');
        
        const filteredMask = [...parseCsvToJson(csvResponseMask)].find(eachMasLine => {
          const [maskCounty] = eachMasLine.split(',');
          if(fips === maskCounty) return eachMasLine;

          return false;
        })

        if (filteredMask) {
          const [,never, rarely, sometimes, frequently, always] = filteredMask.split(',');
          
          return {fips, county, state, 
                  never: Number(never), 
                  rarely: Number(rarely), 
                  sometimes: Number(sometimes), 
                  frequently: Number(frequently), 
                  always: Number(always)
          };
          
        }

        return {county, state};
      });

      const uniq = combineData.reduce((unique, o) => {
        if(!unique.some(obj => obj.county === o.county)) {
          unique.push(o);
        }
        return unique;
      },[]);
      
      setLoading(false);
      setCsv(uniq)
    }  

    getCsvData();
  }, [size]);

  const sortedArray = useMemo(() => {
    const customSort = ( a, b ) => {
      if ( a[selectedSort] < b[selectedSort] ){
        return -1;
      }
      if ( a[selectedSort] > b[selectedSort] ){
        return 1;
      }
      return 0;
    }

    const sortedByField = csv.sort( customSort );

    return sortedByField;

  }, [csv, selectedSort])

  const handleCsvSize = ({target: {value}}) => {
    setSize(Number(value))
  }

  const handleCsvSort = ({target: {value}}) => {
    setSelectedSort(value)
  }

  return {sortedArray, handleCsvSize, handleCsvSort, loading}
}

export { useCsv };